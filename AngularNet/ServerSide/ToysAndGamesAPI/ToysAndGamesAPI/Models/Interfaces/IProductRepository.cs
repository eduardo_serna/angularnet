﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ToysAndGamesAPI.Models.Interfaces
{
    public interface IProductRepository
    {
        Task<IEnumerable<Product>> GetProducts();
        Task<Product> GetProduct(long id);
        Task<int> PutProduct(long id, Product product);
        Task<int> PostProduct(Product product);
        Task<Product> DeleteProduct(long id);
        Task<Product> FindProduct(long id);
        Task<bool> ProductExists(long id);
    }
}
