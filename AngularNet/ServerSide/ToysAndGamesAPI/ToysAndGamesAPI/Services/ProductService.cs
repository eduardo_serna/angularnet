﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ToysAndGamesAPI.Models.Interfaces;

namespace ToysAndGamesAPI.Services
{
    public class ProductService : IProductService
    {
        private IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public async Task<IEnumerable<Product>> GetProducts()
        {
            return await _productRepository.GetProducts();
        }

        public async Task<Product> GetProduct(long id)
        {
            return await _productRepository.GetProduct(id);
        }

        public async Task<int> PutProduct(long id, Product product)
        {
            return await _productRepository.PutProduct(id, product);
        }

        public async Task<int> PostProduct(Product product)
        {
            return await _productRepository.PostProduct(product);
        }

        public async Task<Product> DeleteProduct(long id)
        {
            return await _productRepository.DeleteProduct(id);
        }

        public async Task<Product> FindProduct(long id)
        {
            return await _productRepository.FindProduct(id);
        }

        public async Task<bool> ProductExists(long id)
        {
            return await _productRepository.ProductExists(id);
        }
    }
}
