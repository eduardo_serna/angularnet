import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { InventoryManagementComponent } from './pages/inventory-management/inventory-management.component';
import { ButtonDefault } from './components/button-default/button-default.component';
import { TableDefault } from './components/table-default/table-default.component';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductService } from './services/products.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    InventoryManagementComponent,
    ButtonDefault,
    TableDefault
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  entryComponents: [],
  providers: [
    NgbActiveModal,
    FormBuilder,
    ProductService
  ],
  bootstrap: [InventoryManagementComponent]
})
export class AppModule { }
