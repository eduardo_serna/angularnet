import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
  selector: 'button-default',
  templateUrl: './button-default.component.html'
})
export class ButtonDefault {
  @Input() buttonText: string;
  @Input() buttonType: string;
  @Output() eventClick = new EventEmitter();

  onClick() {
    this.eventClick.emit();
  }
}
