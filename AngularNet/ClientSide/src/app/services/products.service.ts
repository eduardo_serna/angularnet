import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from '../model/Product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private readonly URL = 'https://localhost:5000/api/';

  constructor(private http: HttpClient) { }

  getProducts(): Observable<any> {
    return this.http.get(this.URL + 'Product');
  }

  saveUpdateProduct(item: Product, id: number = 0): Observable<any> {
    let headers = { 'Content-Type': 'application/json' };
    const json = JSON.stringify(item);
    const url = id > 0 ? this.URL + 'Product/' + id : this.URL + 'Product';
    return id > 0 ? this.http.put(url, json, {'headers': headers}) 
                  : this.http.post(url, json, {'headers': headers});
  }

  deleteProduct(id: string) {
    // eserna: I wish I have more time to implement confirmation dialog
    return this.http.delete(this.URL + 'Product/' + id);
  }
}
