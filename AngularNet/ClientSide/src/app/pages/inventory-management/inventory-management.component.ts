import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ButtonType } from '../../constants/enums/button-type.enum';
import {  FormGroupDirective, FormControl, NgForm, FormGroup } from '@angular/forms';
import { ProductService } from '../../services/products.service';
import { Product } from '../../model/Product';

@Component({
  selector: 'inventory-management',
  templateUrl: './inventory-management.component.html',
  styleUrls: ['./inventory-management.component.scss']
})
export class InventoryManagementComponent implements OnInit {
  @ViewChild('modalAddUpdate', { read: ViewContainerRef }) container: ViewContainerRef;
  public form: FormGroup;

  title = 'Inventory Management';
  titleModal = 'Create new product';
  tableCols = ['Id','Name','Age', 'Price', 'Company', 'Options'];
  statusEnum: typeof ButtonType = ButtonType;
  isSubmit: boolean = false;
  invalidPrice: boolean = false;
  rangevalue: number = 0;
  data: any;
  product = {} as Product;
  id: number = 0;

  name = new FormControl('');
  price = new FormControl('');
  description = new FormControl('');
  ageRestriction = new FormControl('');
  company = new FormControl('');

  constructor(private modalService: NgbModal, private productService: ProductService) { }

  ngOnInit(): void {
    this.getProducts();
  }

  addItem(modalContent: FormGroupDirective) {
    this.openModal(modalContent);
  }

  async save(form: NgForm) {
    this.isSubmit = true;
    this.invalidPrice = false;
    
    if (form.invalid) {
      return;
    }

    if(form.controls.price.value > 1000) {
      this.invalidPrice = true;
      return;
    }
    
    const product = {} as Product;
    product.id = this.id > 0 ? this.id : undefined;
    product.name = form.controls.name.value;
    product.description = form.controls.description.value;
    product.ageRestriction = Number(this.rangevalue);
    product.company = form.controls.company.value;
    product.price = form.controls.price.value;

    await this.productService.saveUpdateProduct(product, this.id).toPromise();
    this.close();
    this.getProducts();
  }

  valueChangedRange(e) {
    this.rangevalue = e;
  }

  close() {
    this.product = {} as Product;
    this.modalService.dismissAll();
  }

  async getProducts() {
    this.data = await this.productService.getProducts().toPromise();;
  }

  update(event: any, modalContent: FormGroupDirective) {
    this.product.id = event.id;
    this.product.name = event.name;
    this.product.price = event.price;
    this.product.company = event.company;
    this.product.description = event.description;
    this.rangevalue = event.ageRestriction;
    this.id = event.id;

    this.openModal(modalContent);
  }

  openModal (modalContent: FormGroupDirective){
    var modal = this.modalService.open(modalContent);
    modal.result.then(() => { }, () => { this.rangevalue = 0; this.id = 0; })
  }

  async delete(event: any) {
    await this.productService.deleteProduct(event.id).toPromise();
    this.close();
    this.getProducts();
  }
}
