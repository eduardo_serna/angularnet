﻿using System;
using Microsoft.EntityFrameworkCore;

namespace ToysAndGamesAPI.Repositories
{
    public class ToysAndGamesContext : DbContext
    {
        public ToysAndGamesContext(DbContextOptions<ToysAndGamesContext> options)
            : base(options)
        {
        }

        public DbSet<Product> Products { get; set; }
    }
}
