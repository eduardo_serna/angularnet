export enum ButtonType {
    Default = 'btn btn-primary',
    Success = 'btn btn-success'
}
