using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ToysAndGamesAPI
{
    public class Product
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(100)]
        public string Description { get; set; }

        [Range(0, 100)]
        public int AgeRestriction { get; set; }

        [MaxLength(50)]
        public string Company { get; set; }

        [Range(1, 1000)]
        public decimal Price { get; set; }
    }
}
