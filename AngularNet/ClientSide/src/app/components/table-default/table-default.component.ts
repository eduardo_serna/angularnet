import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'table-default',
  templateUrl: './table-default.component.html',
  styleUrls: ['./table-default.component.scss']
})
export class TableDefault {
    @Input() tableCols: string[];
    @Input() data: any;
    @Input() optionsEnable: boolean = false;
    @Output() updateClick = new EventEmitter();
    @Output() deleteClick = new EventEmitter();

  update(id: any) {
    this.updateClick.emit(id);
  }

  delete(id: any) {
    this.deleteClick.emit(id);
  }
}
