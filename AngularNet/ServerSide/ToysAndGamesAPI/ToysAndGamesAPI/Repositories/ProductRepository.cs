﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ToysAndGamesAPI.Models.Interfaces;
using System.Linq;

namespace ToysAndGamesAPI.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private ToysAndGamesContext _context;

        public ProductRepository(ToysAndGamesContext context)
        {
            _context = context;
        }

        // GET: api/ToysAndGames
        [HttpGet]
        public async Task<IEnumerable<Product>> GetProducts() => await _context.Products.ToListAsync();

        // GET: api/ToysAndGames/5
        [HttpGet("{id}")]
        public async Task<Product> GetProduct(long id) => await _context.Products.FindAsync(id);

        // PUT: api/ToysAndGames/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<int> PutProduct(long id, Product product)
        {
            _context.Entry(product).State = EntityState.Modified;
            return await _context.SaveChangesAsync();
        }

        // POST: api/ToysAndGames
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<int> PostProduct(Product product)
        {
            _context.Products.Add(product);
            return await _context.SaveChangesAsync();
        }

        // DELETE: api/ToysAndGames/5
        [HttpDelete("{id}")]
        public async Task<Product> DeleteProduct(long id)
        {
            var product = await _context.Products.FindAsync(id);
            if (product == null)           
                return null;
            
            _context.Products.Remove(product);
            await _context.SaveChangesAsync();

            return product;
        }

        /// <summary>
        /// Find product by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Product> FindProduct(long id)
        {
            return await _context.Products.Where(x => x.Id.Equals(id)).FirstOrDefaultAsync(); ;
        }

        /// <summary>
        /// Check if product exists by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> ProductExists(long id)
        {
            return await _context.Products.Where(x => x.Id.Equals(id)).AnyAsync();
        }
    }
}
